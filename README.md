# Devops Gitlab Expert 2

Projeto para estudo do gitlab


### Day-1
```bash
- Entender o que é o Git
- Entender o que Working Dir, Index e HEAD
- Entender o que é o Gitlab
- Criar um Grupo no Gitlab
- Criar um repositorio Git
- Aprender os comandos básicos para manipulação de arquivos e diretórios no git
- Criar uma branch
- Criar um Merge Request
- Como adicionar um Membro no projeto
- Como fazer o merge na Master/Main
- Como importar um repo do GitHub para o GitLab
- Como associar um repo local com um repo remoto
- Mudar a branch padrao para Main
```
